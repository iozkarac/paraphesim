#################################################################
########################## LIBRARIES ############################
#################################################################

import numpy as np
import pandas as pd
import sys

#################################################################
########################## ARGUMENTS ############################
#################################################################

input_vars = sys.argv[1:]

pca_url = str(input_vars[0])
sex_url = str(input_vars[1])
output_url = str(input_vars[2])

#################################################################
########################## LOAD DATA ############################
#################################################################

# 10 PCA values
df_v = pd.read_csv(pca_url, header=None, index_col=0, delim_whitespace=True)
df_v = df_v.reset_index()
df_v = df_v.rename(columns={0:'FID', 1:'IID'})
df_v = df_v.rename(columns={x:'PCA-%s' % y for x,y in zip(range(2,12),range(1,11))})
df_v = df_v.set_index('FID')
df_v = df_v[['PCA-%s' % x for x in range(1,11)]]

# Sex values
df_u = pd.read_csv(sex_url, header=0, index_col=0, delim_whitespace=True)
df_u['Sex']=df_u['F']==1
df_u['Sex'] = df_u['Sex'].astype(int)
df_u = df_u[['IID', 'Sex']]

# Combine
df_cov = pd.concat([df_u, df_v], axis=1)

#################################################################
######################### UPLOAD DATA ###########################
#################################################################

df_cov.to_csv(output_url, sep=" ")


