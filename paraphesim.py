"""
ParaPheSim - A paradoxical Phenotype Simulator

For more information please see https://git.ecdf.ed.ac.uk/iozkarac/paraphesim

Any comments or questions, feel free to contact iozkarac@ed.ac.uk..
"""

######################################################################
############################## Modules ###############################
######################################################################

import argparse
import os
import sys
import time
import datetime

import pandas as pd
import numpy as np
import random
from pysnptools.snpreader import Bed


######################################################################
############################## Argparse ##############################
######################################################################

# Parser
parser = argparse.ArgumentParser(prog="ParaPheSim", description="A Paradoxical Phenotype Simulator", epilog= "For more information please see https://git.ecdf.ed.ac.uk/iozkarac/paraphesim")

# Main Commands
parser.add_argument("--paradox", help="Example Usage: --paradox <parameters>", action='store_true')
parser.add_argument("--compute_rank", help="Example Usage: --compute_rank", action='store_true')
parser.add_argument("--hack", help="Example Usage: --paradox", action='store_true')

# Arguments specific to --paradox flag
parser.add_argument("--initial_causal", help="Example Usage: --initial_causal <path>. Note that format has to be as follows: no header & second column contains variants info", metavar="<files_path_prefix>")
parser.add_argument("--replicate", help="Example Usage: --replicate 2.")
parser.add_argument("--seed", help="Example Usage: --seed 2.")
parser.add_argument("--seed_list", help="Example Usage: --seed_list 1 100 1000. Note that there is a space between numbers..", type=int, nargs='+')
parser.add_argument("--h_sqrt", help="Example Usage: --h_sqrt 0.3")

# Arguments specific to --hack flag
parser.add_argument("--g", help="Path for genetic component for the simulated trait. Example Usage: --g <g_file_path>", metavar="<g_file_path>")
parser.add_argument("--y", help="Path for simulated trait. Example Usage: --y <y_file_path>", metavar="<y_file_path>")
parser.add_argument("--significant", help="List of significant variants. Example Usage: --significant <y_file_path>", metavar="<y_file_path>")

# Comman Commands
parser.add_argument("--geno", help="Example Usage: --geno <genotype_file_path>", metavar="<genotype_file_path>")
parser.add_argument("--rank", help="Example Usage: --rank 2.")
parser.add_argument("--out", help="output help", default="paraphesim", metavar="<output file name>")

######################################################################
######################### Initial Message ############################
######################################################################

initial_message='''**********************************
A Paradoxical Phenotype Simulator
Version: v.0.0
**********************************

This version performs 3 tasks: 

(i) --paradox: Simulate a continuous trait, under the model y = g + e, with given parameters so that the generated phenotype y has multiple different representations for the underlying genetic component g.

(ii) --hack: Re-simulate already simulated continuous trait so that new version of the same phenotype has different causality perspective which is defined by the user

(iii) --compute_rank : Compute rank of standardise genotype matrix. This is an intermediate step for the two above tasks.

Additional features might be added later..

A manual is available here:
https://git.ecdf.ed.ac.uk/iozkarac/paraphesim

Any question or comment, feel free to contact iozkarac@ed.ac.uk.
__________________________________
'''

######################################################################
############################ Functions ###############################
######################################################################

if __name__ == "__main__":
	args = parser.parse_args()
	print(initial_message)  
	assert args.out is not None, "Output path is required.."

	input_vars = vars(parser.parse_args())
	with open(args.out + ".log", "w") as f:

		f.write("ParaPheSim v.0.0")
		f.write("\nAnalysis Start Time: %s" % datetime.datetime.now().strftime("%A - %d %B %Y - %H:%M:%S"))   
		start_time=time.time()
		f.write("\nWorking Directory: " + str(os.getcwd()))
		f.write("\nInput Variables:\n")
		for key in input_vars:
			if input_vars[key]!= None and type(input_vars[key])!=bool:
				f.write("\t--%s %s\n" % (str(key), str(input_vars[key])))
			elif input_vars[key]!=vars(parser.parse_args([]))[key] and type(input_vars[key])==bool:
				f.write("\t--%s \n" % str(key))

	if args.hack:
		assert not args.compute_rank, "--hack flag shouldn't be accompanied with --compute_rank flag.."
		assert not args.paradox, "--hack flag shouldn't be accompanied with --paradox flag.."
		assert args.geno is not None, "Path for bed file is required.."
		assert args.g is not None, "Path for genetic component details is required.."
		assert args.y is not None, "Path for simulated phenotype is required.."
		assert args.significant is not None, "Path for markers to be defined as (new) causals is required.."
		assert args.rank is not None, "Rank of genotype matrix to be used is required.."
		## Inputs required -> --rank, --geno, --g, --y, --significant, --out

		rank_G = int(args.rank)
		output_causal = args.out + "hacked.causal"
		output_prs = args.out + "hacked.prs"

		df_g = pd.read_csv(args.g, header=0, index_col=0, delim_whitespace=True)
		effects_e = np.array(df_g['Beta'])

		df_y = pd.read_csv(args.y, header=0, index_col=0, delim_whitespace=True)
		genetic_factor = np.array(df_y['g'])

		snpreader = Bed(args.geno + '.bed', count_A1=True)

		# G = snpreader.read().standardize().val
		G_temp = snpreader.read().val
		df = pd.DataFrame(G_temp)
		non_zero_count = np.array([np.count_nonzero(~np.isnan(df[i])) for i in range(G_temp.shape[1])])
		f = np.array([np.sum(df[i]) for i in range(G_temp.shape[1])]) / (2*non_zero_count)
		col_mean = np.nanmean(G_temp, axis=0)
		indices = np.where(np.isnan(G_temp))
		G_temp[indices] = np.take(col_mean, indices[1])
		G = (G_temp - 2*f) / (np.sqrt(2 * f * (1-f)))
		del G_temp, df, non_zero_count, f, col_mean, indices

		df_bim = pd.read_csv(args.geno + '.bim', index_col=None, header=None, delim_whitespace=True)

		df_significant_subset = pd.read_csv(args.significant, index_col=None, header=None, delim_whitespace=True)   # No column and Second column is the variant info
		significant_full = df_significant_subset[1].tolist()

		df_variants_subset = pd.read_csv(args.geno + '.bim', index_col=None, header=None, delim_whitespace=True)
		variants_full = df_variants_subset[1].tolist()

		assert set(significant_full).issubset(variants_full), "selected significant variants to be causal markers have to be subset of variant_list.."

		print('Start..')

		A = G[:,snpreader.sid_to_index(significant_full)]

		rank_A = np.linalg.matrix_rank(A)
		del A
		print('Rank A is calculated..')
		A1_list, A2_list = [], [] # A = [A1 A2] so that A1 is full rank (i.e. rank(A1) = rank(A)) and A2 is redundant info

		A1_temp = G[:,snpreader.sid_to_index([significant_full[0]])]
		A1_list.append(list(snpreader.sid_to_index([significant_full[0]]))[0])
		for i in snpreader.sid_to_index(significant_full[1:]):
			if np.linalg.matrix_rank(A1_temp) == rank_A:
				break
			if np.linalg.matrix_rank(np.hstack((A1_temp,G[:,i:i+1]))) == np.linalg.matrix_rank(A1_temp)+1:   
				A1_temp = np.hstack((A1_temp,G[:,i:i+1]))
				A1_list.append(i)
			else:
				A2_list.append(i)
		print('Indices for A1, A2 are prepared..')
		S,A2 = G[:,A1_list],G[:,A2_list]
		print('Shape of S and A2 are:', S.shape, A2.shape)
		B_list = [] # S = [A1 B]
		A_list = A1_list + A2_list
		G_minus_A___list = [x for x in snpreader.sid if x not in A_list]
		for i in G_minus_A___list:
			if np.linalg.matrix_rank(S) == rank_G:
				break
			if np.linalg.matrix_rank(np.hstack((S,G[:,snpreader.sid_to_index([i])]))) == np.linalg.matrix_rank(S)+1:
				S = np.hstack((S,G[:,snpreader.sid_to_index([i])]))
				B_list.append(snpreader.sid_to_index([i])[0])
		print('Matrix S = [A1 A2] is computed..')
		causal_variants_list_temp = [snpreader.sid[j] for j in A1_list+B_list+A2_list]

		effects_f = np.ones(len(A2_list))
		effects_h = np.linalg.inv(S.T@S)@S.T@(genetic_factor - A2@effects_f)
		effects_temp = np.hstack((effects_h,effects_f))
		S_union_A2 = np.hstack((S,A2))
		print('Causal Markers are prepared..')

		df_causals = pd.DataFrame(data={'ID':causal_variants_list_temp, 'Beta':effects_temp})
		df_causals = df_causals.set_index('ID')

		df_bim = df_bim.rename(columns={1:'ID', 4:'Ref'})
		df_bim = df_bim.set_index('ID')

		df_causals_final = pd.concat([df_causals, df_bim], join='inner', axis=1)
		df_causals_final = df_causals_final[['Ref', 'Beta']]
		df_causals_final.to_csv(output_causal, sep=" ")
		print('Causals are uploaded..')


		df_pheno = pd.read_csv(args.geno + '.fam', index_col=None, header=None, delim_whitespace=True)
		df_pheno = df_pheno.rename(columns={0: "FID", 1: "IID", 2: "Father", 3: "Mother"})
		df_pheno = df_pheno[['FID','IID','Father','Mother']]
		df_prs = df_pheno[['FID','IID','Father','Mother']]
		df_prs = df_prs.set_index('FID')
		df_prs['g'] = S_union_A2@effects_temp

		df_prs['g_original'] = np.round(np.array(df_y['g']),5)
		df_prs.to_csv(output_prs, sep=" ")
		print('Done..')


	if args.compute_rank:
		assert not args.paradox, "--compute_rank flag shouldn't be accompanied with --paradox flag.. The only job of this flag is to compute rank of Genotype Matrix G (either standardised if acoompanied with --normalise flag or non-standardised otherwise)"

		snpreader = Bed(args.geno + '.bed', count_A1=True)

		# G_full = snpreader.read().standardize().val
		G_temp = snpreader.read().val
		df = pd.DataFrame(G_temp)
		non_zero_count = np.array([np.count_nonzero(~np.isnan(df[i])) for i in range(G_temp.shape[1])])
		f = np.array([np.sum(df[i]) for i in range(G_temp.shape[1])]) / (2*non_zero_count)
		col_mean = np.nanmean(G_temp, axis=0)
		indices = np.where(np.isnan(G_temp))
		G_temp[indices] = np.take(col_mean, indices[1])
		G_full = (G_temp - 2*f) / (np.sqrt(2 * f * (1-f)))
		del G_temp, df, non_zero_count, f, col_mean, indices

		rank = np.linalg.matrix_rank(G_full)
		print("Rank is:", rank)
		print("Done..")

		with open(args.out + ".log", "a") as f:
			f.write("\nRank is: %s" % rank)


	if args.paradox:
		print('Simulating a continuous trait...')

		assert args.initial_causal is not None, "Causal Markers selected by user are required for simulating a phenotype.."
		assert int(args.replicate)<21, "We do not recommend using a replicate index greater than 20 due to computational burden. Please use replicate index < 21. Example: --replicate 4"
		
		her = float(args.h_sqrt)		
		rep = int(args.replicate)

		out_causal = args.out + ".causal"
		out_pheno = args.out + ".pheno"
		out_causal_hacked = args.out + ".causal.v%s"
		out_prs_hacked = args.out + ".prs.v%s"

		df_causals_initial = pd.read_csv(args.initial_causal, index_col=None, header=None, delim_whitespace=True)   # No column and Second column is the variant info
		initial_causal_markers = df_causals_initial[1].tolist()
		df_variants_subset = pd.read_csv(args.geno + '.bim', index_col=None, header=None, delim_whitespace=True)
		variants_full = df_variants_subset[1].tolist()        
		assert set(initial_causal_markers).issubset(variants_full), "selected variants to be causal markers have to be subset of variant_list.."
		snpreader = Bed(args.geno + '.bed', count_A1=True)
		
		# G_full = snpreader.read().standardize().val
		G_temp = snpreader.read().val
		df = pd.DataFrame(G_temp)
		non_zero_count = np.array([np.count_nonzero(~np.isnan(df[i])) for i in range(G_temp.shape[1])])
		f = np.array([np.sum(df[i]) for i in range(G_temp.shape[1])]) / (2*non_zero_count)
		col_mean = np.nanmean(G_temp, axis=0)
		indices = np.where(np.isnan(G_temp))
		G_temp[indices] = np.take(col_mean, indices[1])
		G_full = (G_temp - 2*f) / (np.sqrt(2 * f * (1-f)))
		del G_temp, df, non_zero_count, f, col_mean, indices

		effects_initial = np.round(np.random.normal(0,1,len(initial_causal_markers)),6)

		df_pheno = pd.read_csv(args.geno + '.fam', index_col=None, header=None, delim_whitespace=True)
		df_pheno = df_pheno.rename(columns={0: "FID", 1: "IID", 2: "Father", 3: "Mother"})
		df_pheno = df_pheno[['FID','IID','Father','Mother']]

		g = G_full[:,snpreader.sid_to_index(initial_causal_markers)]@effects_initial
		e = np.round(np.random.normal(0,np.std(g) * np.sqrt((1-her**2)/her**2),G_full.shape[0]),6)
		y = g + e

		df_pheno['g'] = np.round(g,6)
		df_pheno['e'] = e
		df_pheno['y'] = np.round(y,6)
		
		df_pheno = df_pheno.set_index('FID')
		df_pheno.to_csv(out_pheno, sep=" ")

		df_causals = pd.DataFrame(data={'ID':initial_causal_markers, 'Beta':effects_initial})
		df_causals = df_causals.set_index('ID')

		df_bim = pd.read_csv(args.geno + '.bim', index_col=None, header=None, delim_whitespace=True)
		df_bim = df_bim.rename(columns={1:'ID', 4:'Ref'})
		df_bim = df_bim.set_index('ID')

		df_causals_final = pd.concat([df_causals, df_bim], join='inner', axis=1)        
		df_causals_final = df_causals_final[['Ref', 'Beta']]
		df_causals_final.to_csv(out_causal, sep=" ")
		print('Causals are uploaded..')
		del df_causals_final, df_causals

		assert G_full.shape[1]>1000, 'Genotype Matrix has less than 1000 columns..'
		
		if args.seed:
			assert rep==1, 'Replication index has to be 1 whenever --seed flag has been used..'
			print("Choosen seed is:", int(args.seed))
			seeds = [int(args.seed)]
		elif args.seed_list:
			seeds = args.seed_list
			print("Chosen seeds are:", seeds)
			assert rep==len(seeds), 'Replication index has to be the same with length of the seed list (i.e. input of --seed_list flag)..'
		else:
			seeds = np.random.choice(list(range(int(G_full.shape[1]/20))), rep, replace=False)  # randomly choose starting seed for the first causal marker

		for seed,j in zip(seeds, range(2,len(seeds)+2)):
			index_causals = [seed]
			S = G_full[:,seed:seed+1]

			if args.rank:
				i = seed+1
				while np.linalg.matrix_rank(S) != int(args.rank):
					if np.linalg.matrix_rank(np.hstack((S,G_full[:,i:i+1]))) == np.linalg.matrix_rank(S)+1:
						S = np.hstack((S,G_full[:,i:i+1]))
						index_causals.append(i)
					i += 1
				print('Linear base is calculated for seed %s' % j)
			else:
				rank_full = np.linalg.matrix_rank(G_full)
				i = seed+1
				while np.linalg.matrix_rank(S) != rank_full:
					if np.linalg.matrix_rank(np.hstack((S,G_full[:,i:i+1]))) == np.linalg.matrix_rank(S)+1:
						S = np.hstack((S,G_full[:,i:i+1]))
						index_causals.append(i)
					i += 1
				print('Linear base is calculated for seed %s' % j)

			causal_markers = [snpreader.sid[x] for x in index_causals]
			effects = np.linalg.inv(S.T@S)@S.T@g
			print('Causal markers and their effects are ready..')

			df_causals = pd.DataFrame(data={'ID':causal_markers, 'Beta':np.round(effects,6)})
			df_causals = df_causals.set_index('ID')

			df_bim = pd.read_csv(args.geno + '.bim', index_col=None, header=None, delim_whitespace=True)
			df_bim = df_bim.rename(columns={1:'ID', 4:'Ref'})
			df_bim = df_bim.set_index('ID')

			df_causals_final = pd.concat([df_causals, df_bim], join='inner', axis=1)        
			df_causals_final = df_causals_final[['Ref', 'Beta']]
			df_causals_final.to_csv(out_causal_hacked % j, sep=" ")				
			df_prs = df_pheno[['IID','Father','Mother']]
			df_prs['g'] = np.round(S@effects,6)
			df_prs.to_csv(out_prs_hacked % j, sep=" ")
			
			print('Re-simulated phenotype version%s and its causal markers together with associated effect sizes are uploaded..' % j)
		print("Re-simulation process is completed..")

	with open(args.out + ".log", "a") as f:
		f.write("\nAnalysis End Time: %s" % datetime.datetime.now().strftime("%A - %d %B %Y - %H:%M:%S"))
		f.write("\nProgram halted in %s seconds...\n" % (round(time.time() - start_time,2)))



