#!/bin/bash

#################################################################
########################### DIRECTORY ###########################
#################################################################

# Change your directory to a folder of your preference before implementing the following steps

dir_user=`pwd`  # current directory

#################################################################
########################### MODULES #############################
#################################################################

# You need to download PLINK. 
# Please check https://www.cog-genomics.org/plink/ for download options..
# We have used two different versions of PLINK for different tasks
# PLINK v1.90b4 64-bit (20 Mar 2017) and PLINK v2.00a2LM 64-bit Intel (21 May 2018)
# The former is corresponds to the command plink, whereas the latter corresponds to the command plink2 in the following scripts..

# You need to download GCTA (Version: 1.94.0beta - 4 Jan 2022) for GREML calculations. 
# Please check https://yanglab.westlake.edu.cn/software/gcta/#Download for download options..

# You also need Python 

#################################################################
######################## SANITY CHECK ###########################
#################################################################

if test -f ${dir_user}/paraphesim.py
then
  echo "Starting..."
else
  echo "Please download ParaPheSim software to the same folder as this script. Example: git clone https://git.ecdf.ed.ac.uk/iozkarac/project-paraphesim.git"
fi

#################################################################
########## DOWNLOAD AND EXTRACT THE DATA TO A FOLDER ############
#################################################################

cd ${dir_user}

# Download HAPMAP3 dataset
wget https://ftp.ncbi.nlm.nih.gov/hapmap/genotypes/hapmap3_r3/plink_format/hapmap3_r3_b36_fwd.consensus.qc.poly.map.gz
wget https://ftp.ncbi.nlm.nih.gov/hapmap/genotypes/hapmap3_r3/plink_format/hapmap3_r3_b36_fwd.consensus.qc.poly.ped.gz

# Extract HAPMAP3 dataset
gzip -d hapmap3_r3_b36_fwd.consensus.qc.poly.map.gz
gzip -d hapmap3_r3_b36_fwd.consensus.qc.poly.ped.gz

#################################################################
########################### VARIABLES ###########################
#################################################################

# Genotypes
geno=${dir_user}/hapmap3_r3_b36_fwd.consensus.qc.poly         # HAPMAP3 dataset. 
geno_full=${dir_user}/full_genotype                           # This is initial plink format version (bed/bim/fam) of the HAPMAP3 dataset after download. 
geno_1=${dir_user}/geno_filter_1                              # Intermediate variable
geno_2=${dir_user}/geno_filter_2                              # Intermediate variable
geno_3=${dir_user}/HAPMAP3.qc.genotype                        # Final version of genotype file after QC
geno_4=${dir_user}/HAPMAP3.qc.genotype.causals                # Final version of genotype file after QC - Focused on only 1k snps that were chosen randomly from CHR1 to be causal for traits to be simulated
geno_5=${dir_user}/HAPMAP3.qc.genotype.chr1                   # Final version of genotype file after QC - Focused on only chr1 snps
geno_6=${dir_user}/HAPMAP3.qc.genotype.except-causals         # Final version of genotype file after QC - Focused on except causal 1k snps that were chosen randomly from CHR1 to be causal for traits to be simulated
geno_7=${dir_user}/HAPMAP3.qc.genotype.except-chr1            # Final version of genotype file after QC - Focused on except chr1 snps

# Covariates
covars=${dir_user}/HAPMAP3.covars.cov                                                 # Intermediate variable
covars_binary_noheader_temp=${dir_user}/HAPMAP3.binary-covars.noheader.temp           # Intermediate variable
covars_continuous_noheader_temp=${dir_user}/HAPMAP3.quantitative-covars.noheader.temp # Path for all continuous covariates generated for HAPMAP3 dataset (noheader)
covars_binary_noheader=${dir_user}/HAPMAP3.binary-covars.noheader.cov                 # Path for all binary covariates generated for HAPMAP3 dataset (noheader)
covars_continuous_noheader=${dir_user}/HAPMAP3.quantitative-covars.noheader.cov       # Path for all continuous covariates generated for HAPMAP3 dataset (noheader)

# GRMs
grm_full=${dir_user}/GRM/all-variants                            # GRM constructed using all variants
grm_causal=${dir_user}/GRM/only-initial-causal-variants          # GRM constructed using all (initially chosen) 1k causals
grm_chr1=${dir_user}/GRM/only-chr1                               # GRM constructed using chr1 snps
grm_except_causal=${dir_user}/GRM/except-initial-causal-variants # GRM constructed using all variants except initially chosen 1k snps
grm_except_chr1=${dir_user}/GRM/except-chr1                      # GRM constructed using all variants except chr1 snps

# Traits to be simulated - Prefix
qt=${dir_user}/traits/HAPMAP3.phenotype

# ParaPheSim
paraphesim=${dir_user}/paraphesim.py

#################################################################
########################## FILTER DATA ##########################
#################################################################

# convert the file into bed/bim/fam format (using plink v1.90b4)
plink --file ${geno} --make-bed --out ${geno_full}  
# Filter 1: (using plink v2.00a2LM)
plink2  --bfile ${geno_full} --geno 0.1 --hwe 1e-5 --maf 0.48 --mind 0.001  --snps-only --write-samples --write-snplist --out ${geno_1}
plink2  --bfile ${geno_full} --extract ${geno_1}.snplist --keep ${geno_1}.id --make-bed --out ${geno_2}
plink2  --bfile ${geno_2} --autosome --make-bed --out ${geno_3}

#################################################################
################### CONSTRUCT COVARIATES ########################
#################################################################

# Generate 10 PCA values (using plink v1.90b4)
plink --bfile ${geno_3} --pca 10 --out ${geno_3}.pca10

# Generate Sex values (using plink v1.90b4)
plink --bfile ${geno_2} --check-sex --out ${geno_3}.sex

# Construct covars file
python HAPMAP3.construct.covars.py ${geno_3}.pca10.eigenvec ${geno_3}.sex.sexcheck ${covars}

# Construct binary-covars (no header) and quantitative covars (no header)
cp ${covars} ${covars_binary_noheader_temp}
cp ${covars} ${covars_continuous_noheader_temp}
sed -i 1d ${covars_binary_noheader_temp}
sed -i 1d ${covars_continuous_noheader_temp}
cat ${covars_binary_noheader_temp} | awk '{print $1" "$2" "$3}' > ${covars_binary_noheader}
cat ${covars_continuous_noheader_temp} | awk '{print $1" "$2" "$4" "$5" "$6" "$7" "$8" "$9" "$10" "$11" "$12" "$13}' > ${covars_continuous_noheader}

#################################################################
################ SIMULATE CONTINUOUS TRAITS #####################
#################################################################


# Create a folder to store all simulated traits
mkdir -p ${dir_user}/traits

# Choose 1000 random snps
cat ${geno_3}.bim | head -3246 > ${geno_3}.chr1.snplist
shuf ${geno_3}.chr1.snplist > ${geno_3}.chr1.shuffled.snplist
cat ${geno_3}.chr1.shuffled.snplist | head -1000 > ${geno_3}.chr1.shuffled.0-1000.snplist

# Simulate 50 continuous traits using ParaPheSim
# Those traits have 1000 (initial) causal variants that were chosen randomly above
# Each simulated trait then have 5 different replicates with different causality structure than initial choice of causal markers above.
for replicate in {1..50}
do
  python ${paraphesim} --paradox  --geno ${geno_3} --initial_causal ${geno_3}.chr1.shuffled.0-1000.snplist --replicate 5 --seed_list 1 7000 14000 28000 35000 --rank 363 --h_sqrt 0.7 --out ${qt}-${replicate}
done

#################################################################
################## HERITABILITY ESTIMATIONS #####################
#################################################################

# Create plink files
plink2 --bfile ${geno_2} --extract ${geno_3}.chr1.shuffled.0-1000.snplist --make-bed --out ${geno_4}
plink2 --bfile ${geno_2} --extract ${geno_3}.chr1.snplist --make-bed --out ${geno_5}
plink2 --bfile ${geno_2} --exclude ${geno_3}.chr1.shuffled.0-1000.snplist --make-bed --out ${geno_6}
plink2 --bfile ${geno_2} --exclude ${geno_3}.chr1.snplist --make-bed --out ${geno_7}

# Create a folders to store GRMs, GREML and HE-regression results
mkdir -p ${dir_user}/GRM
mkdir -p ${dir_user}/REML
mkdir -p ${dir_user}/HE

# Create GRMs
gcta64 --bfile ${geno_3} --make-grm --thread-num $(nproc) --out ${grm_full}
gcta64 --bfile ${geno_4} --make-grm --thread-num $(nproc) --out ${grm_causal}
gcta64 --bfile ${geno_5} --make-grm --thread-num $(nproc) --out ${grm_chr1}
gcta64 --bfile ${geno_6} --make-grm --thread-num $(nproc) --out ${grm_except_causal}
gcta64 --bfile ${geno_7} --make-grm --thread-num $(nproc) --out ${grm_except_chr1}

echo "REML.."

# GREML and HE
for replicate in {1..50}
do
  # Analysis GREML
  pheno=${qt}-${replicate}.pheno
  gcta64 --reml --pheno ${pheno} --grm ${grm_full} --covar ${covars_binary_noheader} --qcovar ${covars_continuous_noheader} --mpheno 5 --thread-num $(nproc) --out ${dir_user}/REML/HAPMAP3.phenotype-${replicate}.all-variants
  gcta64 --reml --pheno ${pheno} --grm ${grm_causal} --covar ${covars_binary_noheader} --qcovar ${covars_continuous_noheader} --mpheno 5 --thread-num $(nproc) --out ${dir_user}/REML/HAPMAP3.phenotype-${replicate}.only-initial-causal-variants
  gcta64 --reml --pheno ${pheno} --grm ${grm_chr1} --covar ${covars_binary_noheader} --qcovar ${covars_continuous_noheader} --mpheno 5 --thread-num $(nproc) --out ${dir_user}/REML/HAPMAP3.phenotype-${replicate}.only-chr1
  gcta64 --reml --pheno ${pheno} --grm ${grm_except_causal} --covar ${covars_binary_noheader} --qcovar ${covars_continuous_noheader} --mpheno 5 --thread-num $(nproc) --out ${dir_user}/REML/HAPMAP3.phenotype-${replicate}.except-initial-causal-variants
  gcta64 --reml --pheno ${pheno} --grm ${grm_except_chr1} --covar ${covars_binary_noheader} --qcovar ${covars_continuous_noheader} --mpheno 5 --thread-num $(nproc) --out ${dir_user}/REML/HAPMAP3.phenotype-${replicate}.except-chr1

  # Analysis HEreg
  gcta64 --HEreg --pheno ${pheno} --grm ${grm_full} --covar ${covars_binary_noheader} --qcovar ${covars_continuous_noheader} --mpheno 5 --thread-num $(nproc) --out ${dir_user}/HE/HAPMAP3.phenotype-${replicate}.all-variants
  gcta64 --HEreg --pheno ${pheno} --grm ${grm_causal} --covar ${covars_binary_noheader} --qcovar ${covars_continuous_noheader} --mpheno 5 --thread-num $(nproc) --out ${dir_user}/HE/HAPMAP3.phenotype-${replicate}.only-initial-causal-variants
  gcta64 --HEreg --pheno ${pheno} --grm ${grm_chr1} --covar ${covars_binary_noheader} --qcovar ${covars_continuous_noheader} --mpheno 5 --thread-num $(nproc) --out ${dir_user}/HE/HAPMAP3.phenotype-${replicate}.only-chr1
  gcta64 --HEreg --pheno ${pheno} --grm ${grm_except_causal} --covar ${covars_binary_noheader} --qcovar ${covars_continuous_noheader} --mpheno 5 --thread-num $(nproc) --out ${dir_user}/HE/HAPMAP3.phenotype-${replicate}.except-initial-causal-variants
  gcta64 --HEreg --pheno ${pheno} --grm ${grm_except_chr1} --covar ${covars_binary_noheader} --qcovar ${covars_continuous_noheader} --mpheno 5 --thread-num $(nproc) --out ${dir_user}/HE/HAPMAP3.phenotype-${replicate}.except-chr1
  
  # Extract results - GREML
  cat ${dir_user}/REML/HAPMAP3.phenotype-${replicate}.all-variants.hsq | grep "V(G)/Vp" | awk '{print $1" "$2" "$3}' >> ${dir_user}/REML/REML-results.all-variants.hsq
  cat ${dir_user}/REML/HAPMAP3.phenotype-${replicate}.only-initial-causal-variants.hsq | grep "V(G)/Vp" | awk '{print $1" "$2" "$3}' >> ${dir_user}/REML/REML-results.only-initial-causal-variants.hsq
  cat ${dir_user}/REML/HAPMAP3.phenotype-${replicate}.only-chr1.hsq | grep "V(G)/Vp" | awk '{print $1" "$2" "$3}' >> ${dir_user}/REML/REML-results.only-chr1.hsq
  cat ${dir_user}/REML/HAPMAP3.phenotype-${replicate}.except-initial-causal-variants.hsq | grep "V(G)/Vp" | awk '{print $1" "$2" "$3}' >> ${dir_user}/REML/REML-results.except-initial-causal-variants.hsq
  cat ${dir_user}/REML/HAPMAP3.phenotype-${replicate}.except-chr1.hsq | grep "V(G)/Vp" | awk '{print $1" "$2" "$3}' >> ${dir_user}/REML/REML-results.except-chr1.hsq

  # Extract results - HEreg
  cat ${dir_user}/HE/HAPMAP3.phenotype-${replicate}.all-variants.HEreg | head -4 | tail -1 | awk '{print $1" "$2" "$3}' >> ${dir_user}/HE/HEreg-results.all-variants.hsq
  cat ${dir_user}/HE/HAPMAP3.phenotype-${replicate}.only-initial-causal-variants.HEreg | head -4 | tail -1 | awk '{print $1" "$2" "$3}' >> ${dir_user}/HE/HEreg-results.only-initial-causal-variants.hsq
  cat ${dir_user}/HE/HAPMAP3.phenotype-${replicate}.only-chr1.HEreg | head -4 | tail -1 | awk '{print $1" "$2" "$3}' >> ${dir_user}/HE/HEreg-results.only-chr1.hsq
  cat ${dir_user}/HE/HAPMAP3.phenotype-${replicate}.except-initial-causal-variants.HEreg | head -4 | tail -1 | awk '{print $1" "$2" "$3}' >> ${dir_user}/HE/HEreg-results.except-initial-causal-variants.hsq
  cat ${dir_user}/HE/HAPMAP3.phenotype-${replicate}.except-chr1.HEreg | head -4 | tail -1 | awk '{print $1" "$2" "$3}' >> ${dir_user}/HE/HEreg-results.except-chr1.hsq
done

#################################################################
############### UPLOAD HERITABILITY ESTIMATIONS #################
#################################################################

python HAPMAP3.upload-h2-results.py ${dir_user}/REML/REML-results ${dir_user}/h2-REML-final.txt
python HAPMAP3.upload-h2-results.py ${dir_user}/HE/HEreg-results ${dir_user}/h2-HEReg-final.txt

#################################################################
################### REMOVE REDUNDANT FILES ######################
#################################################################

rm ${geno}* ${geno_full}* ${geno_1}* ${geno_2}* ${covars_binary_noheader_temp} ${covars_continuous_noheader_temp}

echo "DONE.."

