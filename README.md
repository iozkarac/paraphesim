# PARAPHESIM

A Paradoxical Phenotype Simulator..

The accompanied paper(s) is/are:

1. Schrödinger’s Cat in Simulations in Genome-wide Association Studies \
https://www.biorxiv.org/content/10.1101/2023.03.22.533184v1

If you have any questions and/or comments please contact iozkarac@ed.ac.uk.


### Download

git clone https://git.ecdf.ed.ac.uk/iozkarac/project-paraphesim.git


### Dependencies
You need to have/install standard Python libraries. Furthermore you need to install pysnptools (pip install pysnptools)

For more information: https://pypi.org/project/pysnptools/


### Current features (v0.0.0)

1. Paradox: Simulate a phenotype with initial choice of causal markers. Then re-simulate the same phenotype n (an integer given by user input) times such that each re-simulated phenotype is generated based on different set of causal markers.

2. Hack: Re-simulate already simulated continuous trait so that new version of the same phenotype has different causality perspective which is defined by the user. This feature can be used to hack summary statistics and to claim that all GWAS hits are true hits with respect to some causality hypothesis (i.e. Schrodinger's Cat in GWAS).

3. Compute rank of Standardised Genotype Matrix: This feature is an intermediate step for other tasks. It is compulsory for --hack flag whereas it is optional (recommended) for --paradox flag.

### Example Usage
```
Generating paradoxical phenotypes:
$ python paraphesim.py \
    --paradox \
    --geno HAPMAP3.qc.genotype \
    --initial_causal HAPMAP3.qc.genotype.chr1.shuffled.0-1000.snplist \
    --replicate 5 \
    --seed_list 1 7000 14000 28000 35000 \
    --h_sqrt 0.7 \
    --rank 363 \
    --out HAPMAP3.phenotype

Hacking summary statistics:
$ python paraphesim.py  --hack \
    --geno HAPMAP3.qc.genotype \
    --g HAPMAP3.phenotype.causal \
    --y HAPMAP3.phenotype \
    --significant HAPMAP3.new-causals.bim \
    --out HAPMAP3.phenotype.hacked

Calculate rank of standardise genotype matrix:
$ python paraphesim.py  --compute_rank \
    --geno HAPMAP3.qc.genotype \
    --out HAPMAP3.qc.genotype.rank

Help:
$ python paraphesim.py --help

More help:
Run "example_usages.sh" file in order to generate examples for each use case scenario of the software.


```


### Input parameters
--geno
 
(String) Path for genotype file. Current version only supports Plink Bed/Bim/Fam format.

--initial_causal

(Bim file format) Variants to be selected as causal markers for continuous trait to be simulated.

--replicate

(Integer) Number of duplicates of the phenotype (with different underlying causal markers)

--seed

(Integer) To choose which SNP to be defined as the first causal marker. Variants are then labelled as causal inductively by their genomic position and according to whether they increase rank of the collection of causal markers. The flag --replicate has to be assigned to 1.

--seed_list

(List) To choose which SNPs to be defined as the first causal markers for multiple re-simulation process.

--g

(String) Path for initially defined causal markers and their effects. This is one of the outputs of the --paradox flag.

--y

(String) Path for simulated pheontype file. This is one of the outputs of the --paradox flag.

--significant

(String) Path for variants to be chosen as new causal markers. Format of the file is exactly the same with bim file format. 

--rank

(Integer) Rank of standardised genotype matrix.

--output

(String) Path to save output file(s).


### Contact
If you have any questions and/or comments about ParaPheSim feel free to contact:

$`\mathbf{Mustafa\ \dot{I}smail\ \ddot{O}zkaraca}`$ (iozkarac@ed.ac.uk).
