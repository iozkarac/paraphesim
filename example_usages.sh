#!/bin/bash

## You need to run the following commands in this script after downloading the git repository 
## (git clone https://git.ecdf.ed.ac.uk/iozkarac/project-paraphesim.git) and running "HAPMAP3.generate.data.sh" file.

dir_user=`pwd`

mkdir -p ${dir_user}/example_usages
cd ${dir_user}/example_usages

# Example: Compute rank of standardise genotype matrix
python ../paraphesim.py --compute_rank --geno ../HAPMAP3.qc.genotype --out ./example.rank

# Example: Simulate paradoxical phenotypes
python ../paraphesim.py --paradox --initial_causal ../HAPMAP3.qc.genotype.chr1.shuffled.0-1000.snplist --replicate 2 --seed_list 1 7000 --h_sqrt 0.7 --geno ../HAPMAP3.qc.genotype --rank 363 --out ./example.paradox

# Example: Hack simulated phenotype
python ../paraphesim.py --hack --significant ../HAPMAP3.qc.genotype.chr1.snplist --rank 363 --geno ../HAPMAP3.qc.genotype --g ./example.paradox.causal --y ./example.paradox.pheno --out ./example.hack

echo "Done.."
