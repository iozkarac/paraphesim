#################################################################
########################## LIBRARIES ############################
#################################################################

import numpy as np
import pandas as pd
import sys

#################################################################
########################## ARGUMENTS ############################
#################################################################

input_vars = sys.argv[1:]
input_url = str(input_vars[0])
output_url = str(input_vars[1])

#################################################################
########################## LOAD DATA ############################
#################################################################

# Urls
v1 = input_url + '.all-variants.hsq'
v2 = input_url + '.except-chr1.hsq'
v3 = input_url + '.except-initial-causal-variants.hsq'
v4 = input_url + '.only-chr1.hsq'
v5 = input_url + '.only-initial-causal-variants.hsq'

# Import dataframes
df1 = pd.read_csv(v1,header=None,index_col=0,delim_whitespace=True)
df2 = pd.read_csv(v2,header=None,index_col=0,delim_whitespace=True)
df3 = pd.read_csv(v3,header=None,index_col=0,delim_whitespace=True)
df4 = pd.read_csv(v4,header=None,index_col=0,delim_whitespace=True)
df5 = pd.read_csv(v5,header=None,index_col=0,delim_whitespace=True)

# Change column names
df1 = df1.rename(columns={1:'h2 - all variants', 2:'SE - all variants'})
df2 = df2.rename(columns={1:'h2 - except chr1', 2:'SE - except chr1'})
df3 = df3.rename(columns={1:'h2 - except initial causal', 2:'SE - except initial causal'})
df4 = df4.rename(columns={1:'h2 - only chr1', 2:'SE - only chr1'})
df5 = df5.rename(columns={1:'h2 - only initial causal', 2:'SE - only initial causal'})

# Concatenate dataframes
df_final = pd.concat([df1,df2,df3,df4,df5], axis=1)

#################################################################
######################### UPLOAD DATA ###########################
#################################################################

df_final.to_csv(output_url, sep=" ")

with open(output_url[:-4] + ".log", 'w') as f:
	f.write("Mean values of columns:\n")
	f.write("%s" % df_final.mean())
	f.write("\nStd values of columns:\n")
	f.write("%s\n" % df_final.std())
